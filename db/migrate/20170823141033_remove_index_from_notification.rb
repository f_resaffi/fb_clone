class RemoveIndexFromNotification < ActiveRecord::Migration[5.1]
  def change
      remove_index :notifications, :post_id
  end
end
