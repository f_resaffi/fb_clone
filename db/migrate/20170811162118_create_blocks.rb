class CreateBlocks < ActiveRecord::Migration[5.1]
  def change
    create_table :blocks do |t|
      t.references :user, foreign_key: true
      t.integer :comment_id
      t.integer :post_id

      t.timestamps
    end
  end
end
