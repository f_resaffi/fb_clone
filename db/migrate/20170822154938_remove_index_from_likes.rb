class RemoveIndexFromLikes < ActiveRecord::Migration[5.1]
  def change
      remove_index :likes, [:likeable_type,:likeable_id]
  end
end
