class RemoveIndexOfLikeableIdFromLikes < ActiveRecord::Migration[5.1]
  def change
      remove_index :likes, name:"index_likes_on_comment_id_and_likeable_id"
      remove_index :likes, name:"index_likes_on_post_id_and_likeable_id"
  end
end
