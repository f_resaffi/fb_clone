class AddPublicProfileToUsers < ActiveRecord::Migration[5.1]
  def change
      add_column :users, :public_profile, :boolean, default: false
  end
end
