class AddIndexToNotifications < ActiveRecord::Migration[5.1]
  def change
      add_index(:notifications, :source_id)
  end
end
