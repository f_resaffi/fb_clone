class AddCommentBoolToBlocks < ActiveRecord::Migration[5.1]
  def change
      add_column :blocks, :comment_block, :boolean, default: false
      add_column :blocks, :post_block, :boolean, default: false
  end
end
