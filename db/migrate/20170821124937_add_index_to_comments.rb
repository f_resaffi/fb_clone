class AddIndexToComments < ActiveRecord::Migration[5.1]
  def change
      add_index :likes, [ :comment_id, :likeable_id ], :unique => true
  end
end
