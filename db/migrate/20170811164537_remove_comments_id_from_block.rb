class RemoveCommentsIdFromBlock < ActiveRecord::Migration[5.1]
  def change
      remove_column :blocks, :comment_id
      remove_column :blocks, :post_id
  end
end
