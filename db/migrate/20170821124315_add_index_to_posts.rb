class AddIndexToPosts < ActiveRecord::Migration[5.1]
  def change
      add_index :likes, [ :post_id, :likeable_id ], :unique => true
  end
end
