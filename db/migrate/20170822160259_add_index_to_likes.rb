class AddIndexToLikes < ActiveRecord::Migration[5.1]
  def change
      add_index :likes, [:likeable_id, :user_id], unique: true
  end
end
