Rails.application.routes.draw do


    ActiveAdmin.routes(self)


    delete 'comments/:id' => 'comments#destroy', format: 'js'


    get 'comments/new'
    #  get 'notifications/create'

    #  get 'notifications/destroy'

    get 'users/show'

    get 'user/show'

    get 'likes/create'

    get 'likes/destroy'

    get '/users/modals/show' => 'comments_modals#show', as: 'show_comment_modal', format: 'js'

    devise_for :users, controllers: { registrations:'registrations', omniauth_callbacks: 'omniauth_callbacks' }

    get 'static_pages/home'
    get 'searches' => 'searches#show'

    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    root "static_pages#home"

    mount ActionCable.server => '/cable'

    resources :posts, only: [:create,:destroy]
    resources :likes, only: [:create,:destroy]
    resources :notifications, only: [:create,:destroy]
    resources :users, only: [:show]
    resources :friendships, only: [:create,:destroy]
    resources :comments, only:[:create,:destroy]
    resources :searches, only:[:show]
    resources :chat_rooms, only: [:new, :create, :show, :index]
end
