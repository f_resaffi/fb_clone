ActiveAdmin.register User do
    permit_params :name,:email,:blocked,:admin

    index do
        selectable_column
        columns_to_exclude = %w(encrypted_password reset_password_token reset_password_sent_at remember_created_at avatar_content_type avatar_file_size avatar_file_name avatar_updated_at)

        (User.column_names - columns_to_exclude).each { |c| column c }
    
        actions defaults: true do |user|
            block_status = user.block_status
            block_status ||= Block.new(user_id:user.id)
            if !block_status.comment_block
                item "Block User from Commenting", block_comment_admin_user_path(user),class: "member_link", method: :put
            else
                item "Unblock User from Commenting", unblock_comment_admin_user_path(user),class: "member_link", method: :put
            end

            if !block_status.post_block
                item "Block User from Posting", block_post_admin_user_path(user),class: "member_link", method: :put
            else
                item "Unblock User from Posting", unblock_post_admin_user_path(user),class: "member_link", method: :put
            end

        end
    end

    member_action :block, method: :put do 
        user = User.find(params[:id])
        user.update(blocked: true)
        redirect_to admin_users_path
    end 
    member_action :block_comment, method: :put do 
        user = User.find(params[:id])
        if !user.block_status.nil?
            user.block_status.update(comment_block:true)
            redirect_to admin_users_path
        else
            block = Block.create(user_id: params[:id], comment_block:true)
            user.block_status = block
            redirect_to admin_users_path
        end 
    end
    member_action :unblock_comment, method: :put do 
        user = User.find(params[:id])
        user.block_status.update(comment_block:false)
        redirect_to admin_users_path
    end 
    member_action :block_post, method: :put do 
        user = User.find(params[:id])
        if !user.block_status.nil?
            user.block_status.update(post_block:true)
            redirect_to admin_users_path
        else
            block = Block.create(user_id: params[:id], post_block:true)
            user.block_status = block
            redirect_to admin_users_path
        end 
    end
    member_action :unblock_post, method: :put do 
        user = User.find(params[:id])
        user.block_status.update(post_block:false)
        redirect_to admin_users_path
    end 
end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

