class LikeDestroyer

    def initialize(id)
        @id = id
    end

    def call
        like_destroy
    end


    private

    attr_reader :id

    def like_destroy
        @like = Like.find(id)
        @like.destroy
    end 
end
