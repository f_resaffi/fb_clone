class FriendshipCreator

    def initialize(user,friend_id)
        @friend_id = friend_id
        @user = user
    end

    def save
      create_friendship! && remove_notifications
    end

    private

    attr_reader :friend_id, :user

    def create_friendship!
      @user.friendships.create(friend_id: friend_id)
    end

    def remove_notifications
      Notification.where(source_id: friend_id, user_id: user.id).delete_all
      true
    end
end
