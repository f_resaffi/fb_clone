class NotificationCreator
    
    def initialize(params)
        @dest_user_id = params[:dest]
        @source_user_id = params[:source]
        @post_id = params[:post_id] 
        @comment_id = params[:comment_id] 

    end

    def call
        create_notification
    end

    private

    attr_reader :comment_id, :dest_user_id, :source_user_id, :post_id

    def create_notification
       dest_user = User.find(dest_user_id) 
       dest_user.notifications.build(notification_params)
    end



    def notification_params
        return { source_id: source_user_id, post_id: post_id, comment_id: comment_id, notification_type: "replied" } if post_id.present?
        return { source_id: source_user_id, notification_type: "friended" } 
    end


end
