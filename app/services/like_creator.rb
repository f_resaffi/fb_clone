class LikeCreator

    def initialize(params, user)
        @comment_id = params[:comment_id]
        @post_id = params[:post_id]
        @user = user
    end

    def persist
        create_like && notify_user
    end
    private

    attr_reader  :comment_id, :post_id, :user

    def create_like
      user.likes.build(hash_for_like).save
    end

    def hash_for_like
     
      return {likeable_type:"Comment",likeable_id: comment_id} if comment_id.present?
      return {likeable_type:"Post",likeable_id: post_id} if post_id.present?
    end

    def notify_user
      true
    end

end
