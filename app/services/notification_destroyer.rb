class NotificationDestroyer

    def initialize(user,source_id)
        @user = user
        @source_id = source_id
    end

    def call
        notification_destroy!
    end


    private

    attr_reader :user, :source_id

    def notification_destroy!
        @notification = @user.notifications.find_by(source_id: @source_id)
        @notification.destroy 
    end



end
