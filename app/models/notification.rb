class Notification < ApplicationRecord
  enum status: [:unread, :read]
  enum notification_type: [:friended,
                           :replied]
  belongs_to :user  
  belongs_to :post, optional: true
  belongs_to :comment, optional: true
  validates :source_id, uniqueness: {scope: :user_id, message: "only one frendship request from user"}, presence: true

  validate :source_id_different_of_user_id

  def source_id_different_of_user_id
        errors.add(:source_id, " source_id can't be the same as the user_id") unless source_id != user_id
  end

end

