class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    # Block permissions
       can :create, :all
       can :read, User.public_profile
       user ||= User.new.decorate # guest user (not logged in)
       block_status = user.block_status
       block_status ||= Block.new(user_id: user.id)

       if block_status.comment_block
         cannot :create, Comment
       end

       if block_status.post_block
         cannot :create, Post
       end

   # User permissions

       User.where(:public_profile => false).each do |u|
           all_friends = user.decorate.all_friends.map(&:id)
           if all_friends.include?(u.id) 
               can :read, u.decorate
           else
               cannot :read, u.decorate
           end
       end 
       can :read, user.decorate
    #
    #
    #
    #
    #
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
