class Comment < ApplicationRecord
  belongs_to :post
  belongs_to :user
  has_many :likes, as: :likeable, dependent: :destroy
  validates :content, length: { maximum: 1000 }, presence: true
  after_commit :notify
#  delegate :notify, to: :NotificationCreator
  default_scope -> { order(created_at: :desc) }


  def notify
      params = {source: self.user.id, dest: self.post.user.id,post_id: self.post.id, comment_id: self.id }
      NotificationCreator.new(params).call.save
  end
end
