class User < ApplicationRecord
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable,
        :recoverable, :rememberable, :trackable, :validatable, :omniauthable, omniauth_providers: [:google_oauth2]
    has_many :posts, dependent: :destroy
    has_many :comments, dependent: :destroy
    has_many :likes, dependent: :destroy
    has_many :notifications
    has_many :friendships, class_name: "Friendship"
    has_many :friends, through: :friendships
    has_many :inverse_friendships, :class_name => "Friendship", :foreign_key => "friend_id"
    has_many :inverse_friends, :through => :inverse_friendships, :source => :user
    has_one :block_status, class_name: "Block"
    has_attached_file :avatar, styles: { medium: "200x200>", thumb: "100x100>" }, default_url: "/user_fb_:style.png"
    validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

    scope :public_profile, -> { where public_profile: true }

    def self.search(search)
        where("name LIKE (:search)", search: "%#{search}%")
    end

    def self.from_omniauth(access_token)
        data = access_token.info
        user = User.where(email: data['email']).first

        unless user
            user = User.create(name: data['name'],
                               email: data['email'],
                               password: Devise.friendly_token[0,20]
                              )
        end
        user
    end
end
