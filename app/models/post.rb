class Post < ApplicationRecord
  belongs_to :user
  validates :content, length: {maximum:1000}, presence: true
  has_many :likes, as: :likeable, dependent: :destroy
  has_many :comments, dependent: :destroy
  default_scope -> { order(created_at: :desc) }
end
