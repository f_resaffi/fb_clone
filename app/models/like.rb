class Like < ApplicationRecord
    belongs_to :user
    belongs_to :likeable, counter_cache: true, polymorphic:true
    validates :likeable_id, presence: true, uniqueness: { scope: [:likeable_id,:user_id] }

end
