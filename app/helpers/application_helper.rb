module ApplicationHelper
    def liked_by_user(user,object)
        object.likes.detect {|like| like.user_id == user.id }
    end

    def page_user
        User.find(params[:id])
    end

    def resource_name
        :user
    end

    def resource
        @resource ||= User.new
    end

    def devise_mapping
        @devise_mapping ||= Devise.mappings[:user]
    end
end
