module UsersHelper
    def set_title
        user_friends = current_user.friends.to_a << current_user.inverse_friends.to_a
        user_friends_ids = user_friends.flatten.map(&:id)
        if @user.find_friend_request(current_user.id) || current_user.decorate.find_friend_request(params[:id])
            @title = "Friend request sent"

        elsif user_friends_ids.include?(params[:id].to_i)
            @title = "You are friends"

        else
            @title = "Add as a friend"
        end
    end
end
