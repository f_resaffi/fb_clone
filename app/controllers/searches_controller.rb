class SearchesController < ApplicationController
  def show
      @search_result = 
          if params[:search]
             User.search(params[:search])
          else
              []
          end
  end
end
