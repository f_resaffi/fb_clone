class LikesController < ApplicationController

    def create
      like_creator = LikeCreator.new(params,current_user)
      if like_creator.persist
          flash[:success] = "Post liked"
          redirect_to request.referrer
      else
          flash[:error] = "Something might have gone wrong"
          redirect_to request.referrer
      end
    end

    def destroy
        @like = LikeDestroyer.new(params[:id]).call
        if @like
            flash[:success] = "Post unliked"
            redirect_to request.referrer
        end
    end

end
