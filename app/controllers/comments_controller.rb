class CommentsController < ApplicationController
    def create
        if user_signed_in?
            @comment = current_user.comments.build(comment_params)
            authorize! :create, @comment
            if @comment.save
            else
                flash[:error] = "Could not comment on this post"
                redirect_to root_url
            end
        end
    end

    def destroy
        @comment = Comment.find(params[:id])
        @comment.destroy
    end

    def new
    end

    def comment_params
        params.require(:comment).permit(:content,:post_id)
    end
end
