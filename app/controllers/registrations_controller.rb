class RegistrationsController < Devise::RegistrationsController
    include ApplicationHelper
    def new
    end
    def create
        @user = User.create(user_params)
        if @user.save
            sign_in(:user,@user)
            redirect_to root_url
        else
            render 'new'
        end
     
    end

    def update
        if current_user.update(user_params)
            redirect_to root_url
        end
    end

    def user_params
        params.require(:user).permit(:name,:email,:avatar,:password,:password_confirmation,:public_profile).reject{|_, v| v.blank?}
    end
end
