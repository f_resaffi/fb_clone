class UsersController < ApplicationController
    include UsersHelper
    before_action :authenticate_user!
    def show
        @comment = current_user.comments.build
        @user = User.find(params[:id]).decorate
        @feed_items = @user.timeline
        set_title
    end

end

