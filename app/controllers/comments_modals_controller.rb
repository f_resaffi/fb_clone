class CommentsModalsController < ApplicationController
    def show
        @comment = current_user.comments.build
        @post = Post.find_by_id(params[:post_id])
        respond_to do |format|
            format.js {render 'show_modal'}
            format.html {render partial: 'posts/show_modal'}
        end
    end
end
