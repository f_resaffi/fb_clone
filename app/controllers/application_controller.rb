class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    before_action :configure_permitted_parameters, if: :devise_controller?
    before_action :set_notification, if: :current_user

    protected
    def configure_permitted_parameters
        added_attrs = [:name, :email, :password, :password_confirmation, :remember_me, :avatar, :public_profile]
        devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
        devise_parameter_sanitizer.permit :account_update, keys: added_attrs
    end
    def set_notification
        @notifications = current_user.notifications.includes(:post,:comment).where(status: "unread")
        @notifications = @notifications.zip(User.where(:id => @notifications.pluck(:source_id)))
        friended_type = lambda { |n| n.first.notification_type == "friended" }
        replied_type = lambda { |n| n.first.notification_type == "replied" }
        @friend_requests = @notifications.select &friended_type
        @reply_notifications = @notifications.select &replied_type
    end

    def authenticate_admin_user!
      if current_user.nil? || !current_user.admin?
        redirect_to new_user_session_path
      end
    end
end
