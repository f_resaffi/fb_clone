class NotificationsController < ApplicationController
    def create
        @notification = NotificationCreator.new(params).call
        if @notification.save 
            redirect_to request.referrer
        else
            flash[:error] = "Friend request couldnt be sent"
            redirect_to request.referrer
        end
    end

    def destroy
        @notification = NotificationDestroyer.new(current_user, params[:id]).call
        redirect_to request.referrer
    end

end
