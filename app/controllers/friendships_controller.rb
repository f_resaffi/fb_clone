class FriendshipsController < ApplicationController
  def create
      friendship_creator = FriendshipCreator.new(current_user, params[:friend])
      if friendship_creator.save
          flash[:success] = "Friend added"
          redirect_to request.referrer
      else
          flash[:error] = "Friend could not be added"
          redirect_to request.referrer
      end
  end

  def destroy
  end
end
