class StaticPagesController < ApplicationController
#  before_action :authenticate_user!
  def home
     if user_signed_in?
      @post = current_user.posts.build
      @comment = current_user.comments.build
      @feed_items = current_user.decorate.feed
      @like = current_user.likes.build
     end
  end


end
