class UserDecorator < Draper::Decorator
    delegate_all
    def feed
        Post.where("user_id = :user_id OR user_id IN (:friends_ids) OR user_id IN (:inverse_friends_ids)", friends_ids: object.friend_ids, inverse_friends_ids: object.inverse_friends.ids, user_id: object.id)
            .includes(:user, :likes)
    end

    def timeline
        Post.where(user_id: id).includes(:user,:likes)
    end

    def admin?
        admin
    end

    def all_friends
        friends + inverse_friends
    end

    def find_friend_request(source_id)
        object.notifications.find_by(source_id:source_id, notification_type:"friended")
    end

    def friends_with?(friend)
        friends.find(friend) || inverse_friends.find(friend)
    end


    # Define presentation-specific methods here. Helpers are accessed through
    # `helpers` (aka `h`). You can override attributes, for example:
    #
    #   def created_at
    #     helpers.content_tag :span, class: 'time' do
    #       object.created_at.strftime("%a %m/%d/%y")
    #     end
    #   end

end
