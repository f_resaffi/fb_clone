require 'rails_helper'

RSpec.feature "Users can sign up", js:true do
    let(:user) { FactoryGirl.create(:user) }
    scenario "with valid attr" do
        visit "/"
        click_link "Sign up"
        fill_in "Name", with: "Test User"
        fill_in "Email", with: "user@user.com"
        fill_in "user_password", with: "123456" 
        fill_in "user_password_confirmation", with: "123456" 
        click_button "Sign up"
        expect(page.html).to have_content "Your profile"
    end
end
