FactoryGirl.define do
    factory :user do
        name "Test User"
        sequence(:email) { |n| "user#{n}@example.com" }
        password "123456"
    end
end
