require 'rails_helper'

RSpec.feature "Signed-in users can comment" do
    let!(:user) { FactoryGirl.create(:user) }

    before do
        login_as(user)
        user.posts.create(content:"post")
    end
    scenario "User creates a post and a comment", js: true do
        visit '/'
        comment = page.find("a", text: "Comments")
        comment.click
        comment_field = page.find("textarea")
        comment_field.set("new lorem ipsum comment")
        click_button 'create'
        expect(page).to have_content "new lorem ipsum comment"
    end
end
