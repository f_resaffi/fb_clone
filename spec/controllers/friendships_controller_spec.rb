require 'rails_helper'

RSpec.describe FriendshipsController, type: :controller do

    before do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        @post = FactoryGirl.create(:post)
        user = FactoryGirl.create(:user)
        @friend = FactoryGirl.create(:user)
        headers = { "HTTP_REFERER" => "/" }
        request.headers.merge! headers
        sign_in user
    end
    describe "POST #create" do
        it "creates a new friendship" do
            expect {
                post :create,  params: {friend: @friend.id}
            }.to change(Friendship,:count).by(1)
        end
        it "creates an invalid friendship" do
            expect {
                post :create,  params: {friend: nil}
            }.to change(Friendship,:count).by(0)
        end
    end
end

