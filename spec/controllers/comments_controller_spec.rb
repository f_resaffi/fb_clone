require 'rails_helper'

RSpec.describe CommentsController, type: :controller do

    before do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        @post = FactoryGirl.create(:post)
        @comment = FactoryGirl.create(:comment) 
        user = FactoryGirl.create(:user)
        user.posts << @post
        user.comments << @comment
        sign_in user
    end
    describe "POST #create" do
        it "creates a new comment" do
            expect {
                post :create,  params: { comment: {content: "test post",post_id: @post.id  } }
            }.to change(Comment,:count).by(1)
        end
        it "creates a new invalid comment" do
            expect {
                post :create,  params: { comment: {content: "",post_id: nil  } }
            }.to change(Comment,:count).by(0)
        end
    end
    describe "POST #destroy" do
        it "destroys a comment" do
            expect {
                delete :destroy, params: { id: @comment.id }, format: :js
            }.to change(Comment,:count).by(-1)

        end
    end
end

