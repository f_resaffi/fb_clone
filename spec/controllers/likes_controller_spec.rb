require 'rails_helper'

RSpec.describe LikesController, type: :controller do

    before do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        @post = FactoryGirl.create(:post)
        @comment = FactoryGirl.create(:comment)
        @user = FactoryGirl.create(:user)
        headers = { "HTTP_REFERER" => "/" }
        request.headers.merge! headers
        sign_in @user
    end
    describe "POST #create" do
        it "creates a new like for a post" do
            expect {
                post :create,  params: {  post_id: @post.id }
            }.to change(Like,:count).by(1)
        end
        it "creates a new like for a comment" do
           @comment = FactoryGirl.create(:comment)
            expect {
                post :create,  params: { comment_id: @comment.id }
            }.to change(Like,:count).by(1)
        end
        it "creates an invalid like" do
            expect {
                post :create
            }.to change(Like,:count).by(0)
        end
    end
    describe "POST #destroy" do
        it "destroys a new like" do
            like =  Like.create(user_id: @user.id,likeable_type: "Post", likeable_id: @post.id)
            expect {
                delete :destroy, params: { id: like.id }
            }.to change(Like,:count).by(-1)

        end
    end
end

