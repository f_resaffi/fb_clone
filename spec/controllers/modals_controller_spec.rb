require 'rails_helper'

RSpec.describe CommentsModalsController, type: :controller do

    before do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        headers = { "HTTP_REFERER" => "/" }
        request.headers.merge! headers
        sign_in user
    end
    describe "GET #show" do
        it "opens the modal" do
            get :show
            expect(response).to render_template(partial: "posts/_show_modal")
        end
    end
end

