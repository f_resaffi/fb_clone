require 'rails_helper'

RSpec.describe SearchesController, type: :controller do

    before do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        headers = { "HTTP_REFERER" => "/" }
        request.headers.merge! headers
        sign_in user
    end
    describe "GET #show" do
        it "returns the search result" do
            get :show, params: { search: "search" } 
            expect(response).to have_http_status(:ok)
        end
        it "returns empty when search is empty" do
            get :show
            expect(response).to have_http_status(:ok)
        end
    end
end

