require 'rails_helper'

RSpec.describe PostsController, type: :controller do

    before do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        @post = FactoryGirl.create(:post)
        user = FactoryGirl.create(:user)
        user.posts << @post
        sign_in user
    end
    describe "POST #create" do
        it "creates a new post" do
            expect {
                post :create,  params: { post: {content: "test post" } }
            }.to change(Post,:count).by(1)
        end
        it "creates an invalid post" do
            expect {
                post :create,  params: { post: {content: "" } }
            }.to change(Post,:count).by(0)
        end
    end
    describe "POST #destroy" do
        it "destroys a new post" do
            expect {
                delete :destroy, params: { id: @post.id }
            }.to change(Post,:count).by(-1)

        end
    end
end

