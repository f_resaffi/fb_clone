require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do

    before do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        @user = FactoryGirl.create(:user)
        headers = { "HTTP_REFERER" => "/" }
        request.headers.merge! headers
        sign_in user
    end
    describe "GET #home" do
        it "has a 200 status code" do
            get :home, params: { id: @user.id }
            expect(response.status).to eq(200)
        end
    end
end

