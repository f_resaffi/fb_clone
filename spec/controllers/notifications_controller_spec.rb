require 'rails_helper'

RSpec.describe NotificationsController, type: :controller do

    before do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        @post = FactoryGirl.create(:post)
        user = FactoryGirl.create(:user)
        @source = FactoryGirl.create(:user)
        @notification = FactoryGirl.create(:notification)
        @dest = FactoryGirl.create(:user)
        user.notifications << @notification
        headers = { "HTTP_REFERER" => "/" }
        request.headers.merge! headers
        sign_in user
    end
    describe "POST #create" do
        it "creates a new notification" do
            expect {
                post :create,  params: { source: @source.id, dest: @dest.id }
            }.to change(Notification,:count).by(1)
        end
        it "creates an invalid notification" do
            expect {
                post :create,  params: { source: nil, dest: @dest.id }
            }.to change(Notification,:count).by(0)
        end
    end
    describe "POST #destroy" do
        it "destroys a notification" do
            expect {
                delete :destroy, params: { id: @notification.id }
            }.to change(Notification,:count).by(-1)

        end
    end
end

