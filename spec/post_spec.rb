require 'rails_helper'

RSpec.feature "Signed-in users can post" do
    let!(:user) { FactoryGirl.create(:user) }

    before do
        login_as(user)
    end

    scenario "User creates a post" do
        visit '/'
        input_field = page.find('textarea')
        input_field.set("test")
        click_button('Post')
        expect(page).to have_content "test"
    end

    scenario "User deletes a post" do
        visit '/'
        input_field = page.find('textarea')
        input_field.set("lorem ipsum de")
        click_button('Post')
        delete = page.find("a", text: "delete")
        delete.click
        expect(page).to have_content "Post deleted"
    end

end
