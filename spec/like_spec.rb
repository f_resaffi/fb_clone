require 'rails_helper'

RSpec.feature "Signed-in users can like a comment" do
    let!(:user) { FactoryGirl.create(:user) }

    before do
        login_as(user)
    end

    scenario "User likes some post" do
        visit '/'
        input_field = page.find('textarea')
        input_field.set("test")
        click_button('Post')
        like = page.find("a", text: "Like")
        like.click
        expect(page).to have_content "Unlike"
    end

    scenario "User dislikes some post" do
        visit '/'
        input_field = page.find('textarea')
        input_field.set("test")
        click_button('Post')
        like = page.find("a", text: "Like")
        like.click
        unlike = page.find("a", text: "Unlike")
        unlike.click
        expect(page).to_not have_content "Unlike"
    end

end
